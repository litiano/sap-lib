<?php
/**
 * Created by PhpStorm.
 * User: E-Commerce 2
 * Date: 25/11/2016
 * Time: 16:01
 */

namespace Litiano\Sap\IdeHelper;


class IDocument_Lines { /* GUID={248B28DD-1281-4C6C-9780-22D19BA13D1F} */
    /* DISPID=1610612736 */
    function QueryInterface(
        /* VT_PTR [26] [in] --> ? [29]  */ &$riid,
        /* VT_PTR [26] [out] --> VT_PTR [26]  */ &$ppvObj
    )
    {
    }
    /* DISPID=1610612737 */
    /* VT_UI4 [19] */
    function AddRef(
    )
    {
    }
    /* DISPID=1610612738 */
    /* VT_UI4 [19] */
    function Release(
    )
    {
    }
    /* DISPID=1610678272 */
    function GetTypeInfoCount(
        /* VT_PTR [26] [out] --> VT_UINT [23]  */ &$pctinfo
    )
    {
    }
    /* DISPID=1610678273 */
    function GetTypeInfo(
        /* VT_UINT [23] [in] */ $itinfo,
        /* VT_UI4 [19] [in] */ $lcid,
        /* VT_PTR [26] [out] --> VT_PTR [26]  */ &$pptinfo
    )
    {
    }
    /* DISPID=1610678274 */
    function GetIDsOfNames(
        /* VT_PTR [26] [in] --> ? [29]  */ &$riid,
        /* VT_PTR [26] [in] --> VT_PTR [26]  */ &$rgszNames,
        /* VT_UINT [23] [in] */ $cNames,
        /* VT_UI4 [19] [in] */ $lcid,
        /* VT_PTR [26] [out] --> VT_I4 [3]  */ &$rgdispid
    )
    {
    }
    /* DISPID=1610678275 */
    function Invoke(
        /* VT_I4 [3] [in] */ $dispidMember,
        /* VT_PTR [26] [in] --> ? [29]  */ &$riid,
        /* VT_UI4 [19] [in] */ $lcid,
        /* VT_UI2 [18] [in] */ $wFlags,
        /* VT_PTR [26] [in] --> ? [29]  */ &$pdispparams,
        /* VT_PTR [26] [out] --> VT_VARIANT [12]  */ &$pvarResult,
        /* VT_PTR [26] [out] --> ? [29]  */ &$pexcepinfo,
        /* VT_PTR [26] [out] --> VT_UINT [23]  */ &$puArgErr
    )
    {
    }
    /* DISPID=1 */
    function Add(
    )
    {
        /* Add */
    }
    /* DISPID=2 */
    /* VT_I4 [3] */
    /* LineNum */
    var $LineNum;

    /* DISPID=3 */
    /* VT_BSTR [8] */
    /* ItemCode */
    var $ItemCode;

    /* DISPID=3 */
    /* ItemCode */
    var $ItemCode;

    /* DISPID=4 */
    /* VT_BSTR [8] */
    /* ItemDescription */
    var $ItemDescription;

    /* DISPID=4 */
    /* ItemDescription */
    var $ItemDescription;

    /* DISPID=5 */
    /* VT_R8 [5] */
    /* Quantity */
    var $Quantity;

    /* DISPID=5 */
    /* Quantity */
    var $Quantity;

    /* DISPID=6 */
    /* VT_DATE [7] */
    /* ShipDate */
    var $ShipDate;

    /* DISPID=6 */
    /* ShipDate */
    var $ShipDate;

    /* DISPID=7 */
    /* VT_R8 [5] */
    /* Price */
    var $Price;

    /* DISPID=7 */
    /* Price */
    var $Price;

    /* DISPID=8 */
    /* VT_R8 [5] */
    /* PriceAfter */
    var $PriceAfterVAT;

    /* DISPID=8 */
    /* PriceAfter */
    var $PriceAfterVAT;

    /* DISPID=9 */
    /* VT_BSTR [8] */
    /* Currency */
    var $Currency;

    /* DISPID=9 */
    /* Currency */
    var $Currency;

    /* DISPID=10 */
    /* VT_R8 [5] */
    /* Rate */
    var $Rate;

    /* DISPID=10 */
    /* Rate */
    var $Rate;

    /* DISPID=11 */
    /* VT_R8 [5] */
    /* Discount percent */
    var $DiscountPercent;

    /* DISPID=11 */
    /* Discount percent */
    var $DiscountPercent;

    /* DISPID=12 */
    /* VT_BSTR [8] */
    /* Vendor num */
    var $VendorNum;

    /* DISPID=12 */
    /* Vendor num */
    var $VendorNum;

    /* DISPID=13 */
    /* VT_BSTR [8] */
    /* Serial num */
    var $SerialNum;

    /* DISPID=13 */
    /* Serial num */
    var $SerialNum;

    /* DISPID=14 */
    /* VT_BSTR [8] */
    /* Warehouse */
    var $WarehouseCode;

    /* DISPID=14 */
    /* Warehouse */
    var $WarehouseCode;

    /* DISPID=15 */
    /* VT_I4 [3] */
    /* Sales Person code */
    var $SalesPersonCode;

    /* DISPID=15 */
    /* Sales Person code */
    var $SalesPersonCode;

    /* DISPID=16 */
    /* VT_R8 [5] */
    /* Commision percent */
    var $CommisionPercent;

    /* DISPID=16 */
    /* Commision percent */
    var $CommisionPercent;

    /* DISPID=17 */
    /* ? [29] */
    /* Tree type */
    var $TreeType;

    /* DISPID=18 */
    /* VT_BSTR [8] */
    /* Account code */
    var $AccountCode;

    /* DISPID=18 */
    /* Account code */
    var $AccountCode;

    /* DISPID=19 */
    /* ? [29] */
    /* Use base units */
    var $UseBaseUnits;

    /* DISPID=19 */
    /* Use base units */
    var $UseBaseUnits;

    /* DISPID=20 */
    /* VT_BSTR [8] */
    /* Supplier catalog number */
    var $SupplierCatNum;

    /* DISPID=20 */
    /* Supplier catalog number */
    var $SupplierCatNum;

    /* DISPID=21 */
    /* VT_BSTR [8] */
    /* Costing code */
    var $CostingCode;

    /* DISPID=21 */
    /* Costing code */
    var $CostingCode;

    /* DISPID=22 */
    /* VT_BSTR [8] */
    /* Project code */
    var $ProjectCode;

    /* DISPID=22 */
    /* Project code */
    var $ProjectCode;

    /* DISPID=23 */
    /* VT_BSTR [8] */
    /* Bar code */
    var $BarCode;

    /* DISPID=23 */
    /* Bar code */
    var $BarCode;

    /* DISPID=24 */
    /* VT_BSTR [8] */
    /* Vat Group */
    var $VatGroup;

    /* DISPID=24 */
    /* Vat Group */
    var $VatGroup;

    /* DISPID=25 */
    /* VT_R8 [5] */
    /* Height 1 */
    var $Height1;

    /* DISPID=25 */
    /* Height 1 */
    var $Height1;

    /* DISPID=26 */
    /* VT_I4 [3] */
    /* Height 1 unit */
    var $Hight1Unit;

    /* DISPID=26 */
    /* Height 1 unit */
    var $Hight1Unit;

    /* DISPID=27 */
    /* VT_R8 [5] */
    /* Height 2 */
    var $Height2;

    /* DISPID=27 */
    /* Height 2 */
    var $Height2;

    /* DISPID=28 */
    /* VT_I4 [3] */
    /* Height 2 unit */
    var $Height2Unit;

    /* DISPID=28 */
    /* Height 2 unit */
    var $Height2Unit;

    /* DISPID=29 */
    /* VT_R8 [5] */
    /* Length 1 */
    var $Lengh1;

    /* DISPID=29 */
    /* Length 1 */
    var $Lengh1;

    /* DISPID=30 */
    /* VT_I4 [3] */
    /* Length 1 unit */
    var $Lengh1Unit;

    /* DISPID=30 */
    /* Length 1 unit */
    var $Lengh1Unit;

    /* DISPID=31 */
    /* VT_R8 [5] */
    /* Length 2 */
    var $Lengh2;

    /* DISPID=31 */
    /* Length 2 */
    var $Lengh2;

    /* DISPID=32 */
    /* VT_I4 [3] */
    /* Length 2 unit */
    var $Lengh2Unit;

    /* DISPID=32 */
    /* Length 2 unit */
    var $Lengh2Unit;

    /* DISPID=33 */
    /* VT_R8 [5] */
    /* Weight 1 */
    var $Weight1;

    /* DISPID=33 */
    /* Weight 1 */
    var $Weight1;

    /* DISPID=34 */
    /* VT_I4 [3] */
    /* Weight 1 unit */
    var $Weight1Unit;

    /* DISPID=34 */
    /* Weight 1 unit */
    var $Weight1Unit;

    /* DISPID=35 */
    /* VT_R8 [5] */
    /* Weight 2 */
    var $Weight2;

    /* DISPID=35 */
    /* Weight 2 */
    var $Weight2;

    /* DISPID=36 */
    /* VT_I4 [3] */
    /* Weight 2 unit */
    var $Weight2Unit;

    /* DISPID=36 */
    /* Weight 2 unit */
    var $Weight2Unit;

    /* DISPID=37 */
    /* VT_R8 [5] */
    /* Factor 1 */
    var $Factor1;

    /* DISPID=37 */
    /* Factor 1 */
    var $Factor1;

    /* DISPID=38 */
    /* VT_R8 [5] */
    /* Factor 2 */
    var $Factor2;

    /* DISPID=38 */
    /* Factor 2 */
    var $Factor2;

    /* DISPID=39 */
    /* VT_R8 [5] */
    /* Factor 3 */
    var $Factor3;

    /* DISPID=39 */
    /* Factor 3 */
    var $Factor3;

    /* DISPID=40 */
    /* VT_R8 [5] */
    /* Factor 4 */
    var $Factor4;

    /* DISPID=40 */
    /* Factor 4 */
    var $Factor4;

    /* DISPID=41 */
    /* VT_I4 [3] */
    /* Base type */
    var $BaseType;

    /* DISPID=41 */
    /* Base type */
    var $BaseType;

    /* DISPID=42 */
    /* VT_I4 [3] */
    /* Base entry */
    var $BaseEntry;

    /* DISPID=42 */
    /* Base entry */
    var $BaseEntry;

    /* DISPID=43 */
    /* VT_I4 [3] */
    /* Base line */
    var $BaseLine;

    /* DISPID=43 */
    /* Base line */
    var $BaseLine;

    /* DISPID=45 */
    /* VT_R8 [5] */
    /* Volume */
    var $Volume;

    /* DISPID=45 */
    /* Volume */
    var $Volume;

    /* DISPID=46 */
    /* VT_I4 [3] */
    /* Volume unit */
    var $VolumeUnit;

    /* DISPID=46 */
    /* Volume unit */
    var $VolumeUnit;

    /* DISPID=47 */
    /* VT_I4 [3] */
    /* Lines Count */
    var $Count;

    /* DISPID=48 */
    function SetCurrentLine(
        /* VT_I4 [3]  */ $LineNum
    )
    {
        /* Set the current line */
    }
    /* DISPID=49 */
    /* VT_PTR [26] */
    /* Invoke  the user fields object */
    var $UserFields;

    /* DISPID=50 */
    /* VT_R8 [5] */
    /* property Width1 */
    var $Width1;

    /* DISPID=50 */
    /* property Width1 */
    var $Width1;

    /* DISPID=51 */
    /* VT_I4 [3] */
    /* property Width1Unit */
    var $Width1Unit;

    /* DISPID=51 */
    /* property Width1Unit */
    var $Width1Unit;

    /* DISPID=52 */
    /* VT_R8 [5] */
    /* property Width2 */
    var $Width2;

    /* DISPID=52 */
    /* property Width2 */
    var $Width2;

    /* DISPID=53 */
    /* VT_I4 [3] */
    /* property Width2Unit */
    var $Width2Unit;

    /* DISPID=53 */
    /* property Width2Unit */
    var $Width2Unit;

    /* DISPID=54 */
    /* VT_BSTR [8] */
    /* property Address */
    var $Address;

    /* DISPID=54 */
    /* property Address */
    var $Address;

    /* DISPID=55 */
    /* VT_BSTR [8] */
    /* property TaxCode */
    var $TaxCode;

    /* DISPID=55 */
    /* property TaxCode */
    var $TaxCode;

    /* DISPID=56 */
    /* ? [29] */
    /* property TaxType */
    var $TaxType;

    /* DISPID=56 */
    /* property TaxType */
    var $TaxType;

    /* DISPID=57 */
    /* ? [29] */
    /* property TaxLiable */
    var $TaxLiable;

    /* DISPID=57 */
    /* property TaxLiable */
    var $TaxLiable;

    /* DISPID=58 */
    /* ? [29] */
    /* property PickStatus */
    var $PickStatus;

    /* DISPID=59 */
    /* VT_R8 [5] */
    /* property PickQuantity */
    var $PickQuantity;

    /* DISPID=60 */
    /* VT_I4 [3] */
    /* property PickListIdNumber */
    var $PickListIdNumber;

    /* DISPID=61 */
    /* VT_BSTR [8] */
    /* property OriginalItem */
    var $OriginalItem;

    /* DISPID=62 */
    /* ? [29] */
    /* property BackOrder */
    var $BackOrder;

    /* DISPID=62 */
    /* property BackOrder */
    var $BackOrder;

    /* DISPID=63 */
    /* VT_BSTR [8] */
    /* property FreeText */
    var $FreeText;

    /* DISPID=63 */
    /* property FreeText */
    var $FreeText;

    /* DISPID=64 */
    /* VT_I4 [3] */
    /* property ShippingMethod */
    var $ShippingMethod;

    /* DISPID=64 */
    /* property ShippingMethod */
    var $ShippingMethod;

    /* DISPID=65 */
    /* VT_I4 [3] */
    /* property POTargetNum */
    var $POTargetNum;

    /* DISPID=66 */
    /* VT_BSTR [8] */
    /* property POTargetEntry */
    var $POTargetEntry;

    /* DISPID=67 */
    /* VT_BSTR [8] */
    /* property POTargetRowNum */
    var $POTargetRowNum;

    /* DISPID=68 */
    /* VT_PTR [26] */
    /* property SerialNumbers */
    var $SerialNumbers;

    /* DISPID=69 */
    /* VT_PTR [26] */
    /* property BatchNumbers */
    var $BatchNumbers;

    /* DISPID=70 */
    /* VT_PTR [26] */
    /* property Expenses */
    var $Expenses;

    /* DISPID=71 */
    /* ? [29] */
    /* property CorrectionInvoiceItem */
    var $CorrectionInvoiceItem;

    /* DISPID=71 */
    /* property CorrectionInvoiceItem */
    var $CorrectionInvoiceItem;

    /* DISPID=72 */
    /* VT_R8 [5] */
    /* property CorrInvAmountToStock */
    var $CorrInvAmountToStock;

    /* DISPID=72 */
    /* property CorrInvAmountToStock */
    var $CorrInvAmountToStock;

    /* DISPID=73 */
    /* VT_R8 [5] */
    /* property CorrInvAmountToDiffAcct */
    var $CorrInvAmountToDiffAcct;

    /* DISPID=73 */
    /* property CorrInvAmountToDiffAcct */
    var $CorrInvAmountToDiffAcct;

    /* DISPID=74 */
    /* VT_R8 [5] */
    /* property AppliedTax */
    var $AppliedTax;

    /* DISPID=75 */
    /* VT_R8 [5] */
    /* property AppliedTaxFC */
    var $AppliedTaxFC;

    /* DISPID=76 */
    /* VT_R8 [5] */
    /* property AppliedTaxSC */
    var $AppliedTaxSC;

    /* DISPID=77 */
    /* ? [29] */
    /* property WTLiable */
    var $WTLiable;

    /* DISPID=78 */
    /* VT_R8 [5] */
    /* property EqualizationTaxPercent */
    var $EqualizationTaxPercent;

    /* DISPID=79 */
    /* VT_R8 [5] */
    /* property TotalEqualizationTaxFC */
    var $TotalEqualizationTaxFC;

    /* DISPID=80 */
    /* VT_R8 [5] */
    /* property TotalEqualizationTaxSC */
    var $TotalEqualizationTaxSC;

    /* DISPID=81 */
    /* VT_R8 [5] */
    /* property NetTaxAmount */
    var $NetTaxAmount;

    /* DISPID=82 */
    /* VT_R8 [5] */
    /* property NetTaxAmountFC */
    var $NetTaxAmountFC;

    /* DISPID=83 */
    /* VT_R8 [5] */
    /* property NetTaxAmountSC */
    var $NetTaxAmountSC;

    /* DISPID=84 */
    /* VT_R8 [5] */
    /* property LineTotal */
    var $LineTotal;

    /* DISPID=84 */
    /* property LineTotal */
    var $LineTotal;

    /* DISPID=86 */
    /* VT_R8 [5] */
    /* property TaxPercentagePerRow */
    var $TaxPercentagePerRow;

    /* DISPID=86 */
    /* property TaxPercentagePerRow */
    var $TaxPercentagePerRow;

    /* DISPID=87 */
    /* VT_R8 [5] */
    /* property TaxTotal */
    var $TaxTotal;

    /* DISPID=88 */
    /* ? [29] */
    /* property DeferredTax */
    var $DeferredTax;

    /* DISPID=88 */
    /* property DeferredTax */
    var $DeferredTax;

    /* DISPID=89 */
    /* VT_R8 [5] */
    /* property TotalEqualizationTax */
    var $TotalEqualizationTax;

    /* DISPID=90 */
    /* VT_BSTR [8] */
    /* property MeasureUnit */
    var $MeasureUnit;

    /* DISPID=91 */
    /* VT_R8 [5] */
    /* property UnitsOfMeasurment */
    var $UnitsOfMeasurment;

    /* DISPID=92 */
    /* VT_R8 [5] */
    /* property ExciseAmount */
    var $ExciseAmount;

    /* DISPID=92 */
    /* property ExciseAmount */
    var $ExciseAmount;

    /* DISPID=93 */
    /* VT_R8 [5] */
    /* property TaxPerUnit */
    var $TaxPerUnit;

    /* DISPID=94 */
    /* VT_R8 [5] */
    /* property TotalInclTax */
    var $TotalInclTax;

    /* DISPID=95 */
    /* VT_BSTR [8] */
    /* property CountryOrg */
    var $CountryOrg;

    /* DISPID=95 */
    /* property CountryOrg */
    var $CountryOrg;

    /* DISPID=96 */
    /* VT_BSTR [8] */
    /* property SWW */
    var $SWW;

    /* DISPID=96 */
    /* property SWW */
    var $SWW;

    /* DISPID=97 */
    /* ? [29] */
    /* property ConsumerSalesForecast */
    var $ConsumerSalesForecast;

    /* DISPID=97 */
    /* property ConsumerSalesForecast */
    var $ConsumerSalesForecast;

    /* DISPID=98 */
    /* VT_PTR [26] */
    /* property WithholdingTaxLines */
    var $WithholdingTaxLines;

    /* DISPID=99 */
    /* VT_BSTR [8] */
    /* property CFOPCode */
    var $CFOPCode;

    /* DISPID=99 */
    /* property CFOPCode */
    var $CFOPCode;

    /* DISPID=100 */
    /* VT_BSTR [8] */
    /* property CSTCode */
    var $CSTCode;

    /* DISPID=100 */
    /* property CSTCode */
    var $CSTCode;

    /* DISPID=101 */
    /* VT_BSTR [8] */
    /* property Usage */
    var $Usage;

    /* DISPID=101 */
    /* property Usage */
    var $Usage;

    /* DISPID=102 */
    /* ? [29] */
    /* property TaxOnly */
    var $TaxOnly;

    /* DISPID=102 */
    /* property TaxOnly */
    var $TaxOnly;

    /* DISPID=103 */
    /* VT_R8 [5] */
    /* property TaxBeforeDPM */
    var $TaxBeforeDPM;

    /* DISPID=104 */
    /* VT_R8 [5] */
    /* property TaxBeforeDPMFC */
    var $TaxBeforeDPMFC;

    /* DISPID=105 */
    /* VT_R8 [5] */
    /* property TaxBeforeDPMSC */
    var $TaxBeforeDPMSC;

    /* DISPID=106 */
    /* ? [29] */
    /* property TransactionType */
    var $TransactionType;

    /* DISPID=106 */
    /* property TransactionType */
    var $TransactionType;

    /* DISPID=107 */
    /* ? [29] */
    /* property DistributeExpense */
    var $DistributeExpense;

    /* DISPID=107 */
    /* property DistributeExpense */
    var $DistributeExpense;

    /* DISPID=108 */
    /* VT_BSTR [8] */
    /* property ShipToCode */
    var $ShipToCode;

    /* DISPID=108 */
    /* property ShipToCode */
    var $ShipToCode;

    /* DISPID=109 */
    /* VT_R8 [5] */
    /* property RowTotalFC */
    var $RowTotalFC;

    /* DISPID=109 */
    /* property RowTotalFC */
    var $RowTotalFC;

    /* DISPID=110 */
    /* VT_R8 [5] */
    /* property RowTotalSC */
    var $RowTotalSC;

    /* DISPID=111 */
    /* VT_R8 [5] */
    /* property LastBuyInmPrice */
    var $LastBuyInmPrice;

    /* DISPID=112 */
    /* VT_R8 [5] */
    /* property LastBuyDistributeSumFc */
    var $LastBuyDistributeSumFc;

    /* DISPID=113 */
    /* VT_R8 [5] */
    /* property LastBuyDistributeSumSc */
    var $LastBuyDistributeSumSc;

    /* DISPID=114 */
    /* VT_R8 [5] */
    /* property LastBuyDistributeSum */
    var $LastBuyDistributeSum;

    /* DISPID=115 */
    /* VT_R8 [5] */
    /* property StockDistributesumForeign */
    var $StockDistributesumForeign;

    /* DISPID=116 */
    /* VT_R8 [5] */
    /* property StockDistributesumSystem */
    var $StockDistributesumSystem;

    /* DISPID=117 */
    /* VT_R8 [5] */
    /* property StockDistributesum */
    var $StockDistributesum;

    /* DISPID=118 */
    /* VT_R8 [5] */
    /* property StockInmPrice */
    var $StockInmPrice;

    /* DISPID=119 */
    /* ? [29] */
    /* property PickStatusEx */
    var $PickStatusEx;

    /* DISPID=120 */
    function Delete(
    )
    {
        /* method Delete */
    }
    /* DISPID=121 */
    /* VT_I4 [3] */
    /* property VisualOrder */
    var $VisualOrder;

    /* DISPID=122 */
    /* VT_R8 [5] */
    /* property BaseOpenQuantity */
    var $BaseOpenQuantity;

    /* DISPID=123 */
    /* VT_R8 [5] */
    /* property UnitPrice */
    var $UnitPrice;

    /* DISPID=123 */
    /* property UnitPrice */
    var $UnitPrice;

    /* DISPID=124 */
    /* ? [29] */
    /* property LineStatus */
    var $LineStatus;

    /* DISPID=124 */
    /* property LineStatus */
    var $LineStatus;

    /* DISPID=125 */
    /* VT_R8 [5] */
    /* property PackageQuantity */
    var $PackageQuantity;

    /* DISPID=126 */
    /* VT_BSTR [8] */
    /* property Text */
    var $Text;

    /* DISPID=77 */
    /* property WTLiable */
    var $WTLiable;

    /* DISPID=127 */
    /* ? [29] */
    /* property LineType */
    var $LineType;

    /* DISPID=127 */
    /* property LineType */
    var $LineType;

    /* DISPID=128 */
    /* VT_BSTR [8] */
    /* property COGSCostingCode */
    var $COGSCostingCode;

    /* DISPID=128 */
    /* property COGSCostingCode */
    var $COGSCostingCode;

    /* DISPID=129 */
    /* VT_BSTR [8] */
    /* property COGSAccountCode */
    var $COGSAccountCode;

    /* DISPID=129 */
    /* property COGSAccountCode */
    var $COGSAccountCode;

    /* DISPID=130 */
    /* VT_BSTR [8] */
    /* property ChangeAssemlyBoMWarehouse */
    var $ChangeAssemlyBoMWarehouse;

    /* DISPID=130 */
    /* property ChangeAssemlyBoMWarehouse */
    var $ChangeAssemlyBoMWarehouse;

    /* DISPID=81 */
    /* property NetTaxAmount */
    var $NetTaxAmount;

    /* DISPID=82 */
    /* property NetTaxAmountFC */
    var $NetTaxAmountFC;

    /* DISPID=131 */
    /* VT_BSTR [8] */
    /* property ItemDetails */
    var $ItemDetails;

    /* DISPID=131 */
    /* property ItemDetails */
    var $ItemDetails;

    /* DISPID=132 */
    /* VT_DATE [7] */
    /* property ActualDeliveryDate */
    var $ActualDeliveryDate;

    /* DISPID=132 */
    /* property ActualDeliveryDate */
    var $ActualDeliveryDate;

    /* DISPID=133 */
    /* VT_I4 [3] */
    /* property LocationCode */
    var $LocationCode;

    /* DISPID=133 */
    /* property LocationCode */
    var $LocationCode;

    /* DISPID=134 */
    /* VT_BSTR [8] */
    /* property CostingCode2 */
    var $CostingCode2;

    /* DISPID=134 */
    /* property CostingCode2 */
    var $CostingCode2;

    /* DISPID=135 */
    /* VT_BSTR [8] */
    /* property CostingCode3 */
    var $CostingCode3;

    /* DISPID=135 */
    /* property CostingCode3 */
    var $CostingCode3;

    /* DISPID=136 */
    /* VT_BSTR [8] */
    /* property CostingCode4 */
    var $CostingCode4;

    /* DISPID=136 */
    /* property CostingCode4 */
    var $CostingCode4;

    /* DISPID=137 */
    /* VT_BSTR [8] */
    /* property CostingCode5 */
    var $CostingCode5;

    /* DISPID=137 */
    /* property CostingCode5 */
    var $CostingCode5;

    /* DISPID=138 */
    /* VT_R8 [5] */
    /* property RemainingOpenQuantity */
    var $RemainingOpenQuantity;

    /* DISPID=139 */
    /* VT_R8 [5] */
    /* property OpenAmount */
    var $OpenAmount;

    /* DISPID=140 */
    /* VT_R8 [5] */
    /* property OpenAmountFC */
    var $OpenAmountFC;

    /* DISPID=141 */
    /* VT_R8 [5] */
    /* property OpenAmountSC */
    var $OpenAmountSC;

    /* DISPID=90 */
    /* property MeasureUnit */
    var $MeasureUnit;

    /* DISPID=91 */
    /* property UnitsOfMeasurment */
    var $UnitsOfMeasurment;

    /* DISPID=170 */
    /* VT_R8 [5] */
    /* property GrossBuyPrice */
    var $GrossBuyPrice;

    /* DISPID=170 */
    /* property GrossBuyPrice */
    var $GrossBuyPrice;

    /* DISPID=171 */
    /* VT_I4 [3] */
    /* property GrossBase */
    var $GrossBase;

    /* DISPID=171 */
    /* property GrossBase */
    var $GrossBase;

    /* DISPID=172 */
    /* VT_R8 [5] */
    /* property GrossProfitTotalBasePrice */
    var $GrossProfitTotalBasePrice;

    /* DISPID=172 */
    /* property GrossProfitTotalBasePrice */
    var $GrossProfitTotalBasePrice;

    /* DISPID=173 */
    /* VT_BSTR [8] */
    /* property ExLineNo */
    var $ExLineNo;

    /* DISPID=173 */
    /* property ExLineNo */
    var $ExLineNo;

    /* DISPID=181 */
    /* VT_PTR [26] */
    /* property TaxJurisdictions */
    var $TaxJurisdictions;

    /* DISPID=182 */
    /* VT_DATE [7] */
    /* property RequiredDate */
    var $RequiredDate;

    /* DISPID=182 */
    /* property RequiredDate */
    var $RequiredDate;

    /* DISPID=183 */
    /* VT_R8 [5] */
    /* property RequiredQuantity */
    var $RequiredQuantity;

    /* DISPID=183 */
    /* property RequiredQuantity */
    var $RequiredQuantity;

    /* DISPID=184 */
    /* VT_BSTR [8] */
    /* property COGSCostingCode2 */
    var $COGSCostingCode2;

    /* DISPID=184 */
    /* property COGSCostingCode2 */
    var $COGSCostingCode2;

    /* DISPID=185 */
    /* VT_BSTR [8] */
    /* property COGSCostingCode3 */
    var $COGSCostingCode3;

    /* DISPID=185 */
    /* property COGSCostingCode3 */
    var $COGSCostingCode3;

    /* DISPID=186 */
    /* VT_BSTR [8] */
    /* property COGSCostingCode4 */
    var $COGSCostingCode4;

    /* DISPID=186 */
    /* property COGSCostingCode4 */
    var $COGSCostingCode4;

    /* DISPID=187 */
    /* VT_BSTR [8] */
    /* property COGSCostingCode5 */
    var $COGSCostingCode5;

    /* DISPID=187 */
    /* property COGSCostingCode5 */
    var $COGSCostingCode5;

    /* DISPID=87 */
    /* property TaxTotal */
    var $TaxTotal;

    /* DISPID=188 */
    /* VT_BSTR [8] */
    /* property CSTforIPI */
    var $CSTforIPI;

    /* DISPID=188 */
    /* property CSTforIPI */
    var $CSTforIPI;

    /* DISPID=189 */
    /* VT_BSTR [8] */
    /* property CSTforPIS */
    var $CSTforPIS;

    /* DISPID=189 */
    /* property CSTforPIS */
    var $CSTforPIS;

    /* DISPID=190 */
    /* VT_BSTR [8] */
    /* property CSTforCOFINS */
    var $CSTforCOFINS;

    /* DISPID=190 */
    /* property CSTforCOFINS */
    var $CSTforCOFINS;

    /* DISPID=191 */
    /* VT_BSTR [8] */
    /* property CreditOriginCode */
    var $CreditOriginCode;

    /* DISPID=191 */
    /* property CreditOriginCode */
    var $CreditOriginCode;

    /* DISPID=192 */
    /* ? [29] */
    /* property WithoutInventoryMovement */
    var $WithoutInventoryMovement;

    /* DISPID=192 */
    /* property WithoutInventoryMovement */
    var $WithoutInventoryMovement;

    /* DISPID=193 */
    /* VT_I4 [3] */
    /* property AgreementNo */
    var $AgreementNo;

    /* DISPID=193 */
    /* property AgreementNo */
    var $AgreementNo;

    /* DISPID=194 */
    /* VT_I4 [3] */
    /* property AgreementRowNumber */
    var $AgreementRowNumber;

    /* DISPID=194 */
    /* property AgreementRowNumber */
    var $AgreementRowNumber;

    /* DISPID=195 */
    /* VT_BSTR [8] */
    /* property ShipToDescription */
    var $ShipToDescription;

    /* DISPID=195 */
    /* property ShipToDescription */
    var $ShipToDescription;

    /* DISPID=196 */
    /* VT_I4 [3] */
    /* property ActualBaseEntry */
    var $ActualBaseEntry;

    /* DISPID=196 */
    /* property ActualBaseEntry */
    var $ActualBaseEntry;

    /* DISPID=197 */
    /* VT_I4 [3] */
    /* property ActualBaseLine */
    var $ActualBaseLine;

    /* DISPID=197 */
    /* property ActualBaseLine */
    var $ActualBaseLine;

    /* DISPID=198 */
    /* VT_I4 [3] */
    /* property DocEntry */
    var $DocEntry;

    /* DISPID=199 */
    /* VT_PTR [26] */
    /* property ImportProcesses */
    var $ImportProcesses;

    /* DISPID=200 */
    /* VT_PTR [26] */
    /* property ExportProcesses */
    var $ExportProcesses;

    /* DISPID=201 */
    /* VT_R8 [5] */
    /* property Surpluses */
    var $Surpluses;

    /* DISPID=201 */
    /* property Surpluses */
    var $Surpluses;

    /* DISPID=202 */
    /* VT_R8 [5] */
    /* property DefectAndBreakup */
    var $DefectAndBreakup;

    /* DISPID=202 */
    /* property DefectAndBreakup */
    var $DefectAndBreakup;

    /* DISPID=203 */
    /* VT_R8 [5] */
    /* property Shortages */
    var $Shortages;

    /* DISPID=203 */
    /* property Shortages */
    var $Shortages;

}
